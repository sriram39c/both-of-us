from Tkinter import *
import tkFileDialog
import csv
import re
import webbrowser

class Application(Frame):
    def __init__(self, master = None):
		#initializing the frame
        Frame.__init__(self,master,width=100)
        self.grid()
        self.createWidgets()

    def createWidgets(self):
        top = self.winfo_toplevel()
        self.menuBar = Menu(top)
		#creating a menu bar
        top["menu"] = self.menuBar
        self.subMenu = Menu(self.menuBar)
        self.menuBar.add_cascade(label = "Menu", menu = self.subMenu)
		#adding list to the menu bar
        self.subMenu.add_command( label = "import a csv file",command = self.readCSV)
        self.subMenu.add_command( label = "Close window",command=self.quit)


    def readCSV(self):
		#ask user to open a csv file
        self.filename = tkFileDialog.askopenfilename()
        f = open(self.filename,"rb")
        self.file = f.name
        print (f.name)
        read = csv.reader(f, delimiter = ",")
        file_name = re.findall(r'\/(?:.(?!\/))+$',f.name)  
			
        new_btn = Button(self, text="Execute "+file_name[0].replace('/','')+" to create GANTT CHART", command=self.btnClick)
        new_btn.pack()

    def btnClick(self):
		#opening GANTT in webbrowser
        url = self.gantt_chart(self.file)
        webbrowser.open(url)
        
    def gantt_chart(self,filename):
        import plotly
        import plotly.plotly as py
        import plotly.figure_factory as ff
        import pandas as pd
        import datetime
		# reading the imported file
        data= pd.read_csv(self.filename)
        print (data)
		######API KEY SETUP ####
        # to create  api key visit the following link - signup and create user name - https://plot.ly/settings/api#/
        plotly.tools.set_credentials_file(username='YOUR USER NAME', api_key='YOUR API KEY')
        list_of_dict = [] 
        for index,rows in data.iterrows():
            item = {}
            item = rows.to_dict()
            item['Complete'] = item['Complete']
            date_time = lambda data :datetime.datetime.strptime(data,'%d-%m-%Y').strftime('%Y-%m-%d')    
            item['Start'] = date_time(item['Start'])    
            item['Finish'] = date_time(item['Finish'])  
            list_of_dict.append(item)
        fig = ff.create_gantt(list_of_dict,title='Employee work track',show_colorbar=True,index_col='Complete',
                              bar_width=0.1, showgrid_x=True, showgrid_y=True)
        plotly_link = py.plot(fig,filename='privacy-secret',sharing='public',world_readable=True,auto_open=False)    
        return plotly_link
        
        

app = Application()
app.master.title("test")
app.mainloop()