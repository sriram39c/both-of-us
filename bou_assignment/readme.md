## The program in written in Python Language. Hence set up a python environment and follow the instructions
1. Import a csv file
2. CSV file must have Task,Start,Finish,Complete,Resource columns
3. Start and Finish date format should be 'dd-mm-yyyy'
4. open command prompt
5. locate to the directory in which the assignment is downloaded
## Program has used plotly charts to display GANTT chart.So,before running the program 
	open main.py in any default editor -> within gantt_chart function -->
	create your own API credentials with the instructions given in the code as a comment.
6. type: python main.py
7. install the dependencies by using the command pip install package_name
8. click Menu->import a csv file -> Execute "file name" to GANTT CHART (Kept a sample csv file named: 'sample_data.csv')
9. output will get open on a default browser 
10. attached snapshots of the output in screenshots folder
Note: written in python 2.7. Will work in advanced versions as well.
